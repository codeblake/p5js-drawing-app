# **Drawing App - Report**

![DrawingApp](media/images/drawingApp.png)

## **Table of Contents**

- [**Drawing App - Report**](#drawing-app---report)
    - [**Table of Contents**](#table-of-contents)
    - [**Summary**](#summary)
    - [**Tools**](#tools)
        - [**Eraser**](#eraser)
        - [**Shapes (Rectangle, Ellipse, Triangle)**](#shapes-rectangle--ellipse--triangle)
        - [**Star Spray**](#star-spray)
        - [**Stamp**](#stamp)
        - [**Pencil**](#pencil)
        - [**Bucket**](#bucket)
    - [**Features**](#features)
    - [**Challenges**](#challenges)
        - [**Loading an Image**](#loading-an-image)
        - [**The Bucket Tool**](#the-bucket-tool)
        - [**The Fractal Tool**](#the-fractal-tool)
    - [**What I Would Have Done Differently**](#what-i-would-have-done-differently)
    - [**Wiki**](#wiki)

## **Summary**

For this assignment I decided to build upon the drawing application provide from `case study 1`. The goal was to extend the application by adding the tools and features listed below.

---

## **Tools**

### **Eraser**

<img src="/media/icons/eraserTool.png" width=50>

The _eraser_ tool erases any pixels draw on the canvas and its size can be set by the brush size slider.

This is achieved by drawing a line and setting the colour of the eraser to match the background colour.

### **Shapes (Rectangle, Ellipse, Triangle)**

<img src="/media/icons/rectTool.png" width=50>
<img src="/media/icons/ellipseTool.png" width=50>
<img src="/media/icons/triangleTool.png" width=50>

The _rectangle, ellipse_ and _triangle_ tool allows the user to manipulate the width and height of a shape by dragging the mouse on the canvas whilst holding the mouse button. It completes the process once the mouse button is releasing.

This is achieved by recording the starting position of the initial mouse click and using `loadPixels()` in conjunction with `updatePixels()` to render the shape when the mouse button is released.

### **Star Spray**

<img src="/media/icons/starSprayTool.png" width=50>

The _star spray_ tool allows the user to draw several stars at the mouse location when the mouse button is pressed. The spread and size of the spray are set by the toolbar options on the right.

This was achieved with a combination of the spray tool and the help of the [star][] function provided by the p5js website.

### **Stamp**

<img src="/media/icons/stampTool.png" width=50>

The _stamp_ tool allows the user to place a predefined image onto the mouse location. There are 20 images to select from the right-hand side toolbar, with options to set the size of the image.

This was achieved by using an embedded expression with a _template literal_ to load the images, which were provided free by [pngpix][].

### **Pencil**

<img src="/media/icons/pencilTool.png" width=50>

The _pencil_ tool allows the user to draw a stroke in the style of a pencil, which can also be set to the desired colour.

This was achieved with the help of a p5js library called scribble and with the use of _Processing's_ pmouseX and pmouseY system variables.

### **Bucket**

<img src="/media/icons/bucketTool.png" width=50>

The _bucket_ tool allows the user to replace a colour on the canvas, with the colour selected by the colour picker.

This was achieved by writing an algorithm which scans from the user's mouse location for the colour to replace and adds the matching pixels into an array. After the scan, the function replaces all the pixels in the array with the selected colour requested by the user.

---

## **Features**

Other features implemented include:

- Stroke toggle
- Stroke & brush size
- Colour picker
- Preview box
- UI updates
- Image Filters

---

## **Challenges**

There have been challenges during the development of this application. Particularly when loading an image created by the user, as well as, implementing the bucket tool.

### **Loading an Image**

I wanted an option to load the image the user had created and saved from a previous sketch. Unfortunately, I could not find a way to make this work well with the current [save][] function and [loadImage][] function provided by the _p5js_ library.

```javascript
//Save Function
save([objectOrFilename],[filename],[options])
```

This is due to the fact that I couldn't find a way to state the location to save the image, as the parameters _(provided above)_ only specify the object, name and file type.

```javascript
//Load Function
loadImage(path,[successCallback],[failureCallback])
```

This wouldn't have been a problem if `save(filename)` would have recorded the image into the root directory, as the [load image][] function loads an image from the root directory specified by the path parameter.

### **The Bucket Tool**

During the development of the bucket tool, I created an algorithm to scan and check the pixels for the colour to replace, starting from the where the user clicks. If it finds the colour, it stores the `x` and `y` coordinates into an array called `pixelStack`. This is then later used as a parameter for the function `this.replacePixelColour()` to replace the colour of the pixels found in the array.

Below is a snippet of said algorithm. It checks the upper and lower sides for both the left and right sides, stopping at a pixel that does not match the target colour specified.

```javascript
//CHECK UPPER Left SIDE-------------------
while(y > 0){

    while(x > 0){

        //get pixel colour
        pixelColour = get(x,y);

        //check colours (Underscore lib function to compare colour)
        if(_.isEqual(pixelColour,targetColour)){

            //set pixel loc to stack if same colour
            pixelStack.push([x,y]);

            //move x-pos left
            x--;
        }
        else{
            //exit when not colour
            break;
        }
    }

    //stop searching y if top pixel not matching colour (if x reset)
    if (x == startingPointX){
        break;
    }
    else{
        //reset x pos
        x = startingPointX;

        //change row
        y--;
    }
}

//reset y
y = startingPointY;

...

```

Once the function has finished checking the upper and lower, left and right sides, it passes the `pixelStack` array into the function below, which replaces the colours of those pixels.

```javascript
//Replaces the colour of the pixel from the pixelStack
this.replacePixelColour = function(stack){

    console.log("replacing...")

    col = color(rgb); //get rgb as new colour

    for (let i=0; i < stack.length; i++){

        //change pixel's colour
        set(stack[i][0],stack[i][1],col.levels);
    }
    updatePixels();
};
```

The issue I came across was that this implementation was _very_ slow and the larger the surface area was, the longer it would take to complete the algorithm. This didn't make any sense as _p5js'_ `background()` and bucket/fill tools in other applications where processed far quicker.

After an investigation as to why this was the case, I found that the [get][] function call was the culprit. On the _p5js_ website, it stated that:

>"Getting the color of a single pixel with `get(x, y)` is easy, but not as fast as grabbing the data directly from `pixels[]`.

I researched the reference documentation for the [pixel array][] and implemented the change in my code. I also simplified the code by nesting the left and right side _while loops_. This way they iterate the left and right sides for the upper quadrants, then repeat the process for the lower quadrants.

Just like the previous version, it repeats for the lower quadrants and calls the `this.replacePixelColour()` method but this time it executes _much faster_!

### **The Fractal Tool**

Fractal trees are a representation of data that is used in a lot of ways. from finding all possible outcomes. Such structure can be used to solve a game such as checkers. It is a way that solving a game can be represented.
A version of fractal trees is used for [factoring Factorials][5]. It is a way to factorise a number into its prime numbers.
Another use for this could be to create randomly or with options of customization generated trees in 3-Dimetional game world.
For this tool I wanted to play with this theorem. Lear and explore ways it can be used. Which has many real world uses.

> Further description available at [learn.gold][]

## **What I Would Have Done Differently**

I would have made better use of the _p5.dom_ and _p5.gui_ libraries, as I spent a lot of time programming user input functions, as well as, styling the user interface.

## **Wiki**

Wiki available at [learn.gold][]

[loadImage]:    https://p5js.org/reference/#/p5/loadImage             "p5js load function"
[save]:         https://p5js.org/reference/#/p5/save                  "p5js save function"
[star]:         https://p5js.org/examples/form-star.html              "p5js star function"
[get]:          https://p5js.org/reference/#/p5/get                   "p5js get function"
[pixelArray]:   https://p5js.org/reference/#/p5/pixels                "p5js pixel array"
[learn.gold]:   https://learn.gold.ac.uk/mod/wiki/view.php?id=514622  "learn.gold"
[pngpix]:       http://www.pngpix.com/                                "PngPix"

[5]: http://mathforum.org/library/drmath/view/58522.html