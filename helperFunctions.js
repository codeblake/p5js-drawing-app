var rgb = [0, 0, 0];
var stroke_rgb = [0, 0, 0];
var strokeSize = 1;

function HelperFunctions() {

    //Jquery click events. Notice that there is no this. at the
    //start we don't need to do that here because the event will
    //be added to the button and doesn't 'belong' to the object

    //event handler for the clear button event. Clears the screen
    $("#clearButton").on("click", function () {
        background(255);
        //call loadPixels to update the drawing state
        //this is needed for the mirror tool
        loadPixels();
    });

    //event handler for the save image button. saves the canvsa to the
    //local file system.
    $("#saveImageButton").on("click", function () {

        //get filename
        filename = prompt("Enter a filename:", "image.png")

        //check valid input
        if (filename == "") {
            alert("Invalid filename")
        }
        else if (filename) {
            save(filename);
            alert(filename + " saved!")
        }
    });

    //FILTER click events
    $("#filters").on("change", function () {
        var filterMenu = document.getElementById("filters");
        var filterType = filterMenu.options[filterMenu.selectedIndex].value;

        if(filterType != "NONE"){   //if selected a filter, enable said filter
            switch (filterType) {
                case "POSTERIZE":
                    filter(filterType, 10);
                    break;
                case "BLUR":
                    filter(filterType, 3);
                    break;
                default:
                    filter(filterType);
            }
        }
    });

    var colourPreview = document.getElementById("colourPreview");

    //SLIDERS:
    //rgb
    var redSlider = document.getElementById("redSlider");
    var greenSlider = document.getElementById("greenSlider");
    var blueSlider = document.getElementById("blueSlider");

    //stroke
    var strokeSlider = document.getElementById("strokeSlider");

    //stamp
    var stampSizeSlider = document.getElementById("stampSizeSlider");

    //brush
    var brushSizeSlider = document.getElementById("brushSizeSlider");

    //spray
    var spraySizeSlider = document.getElementById("spraySizeSlider");
    var spraySpreadSlider = document.getElementById("spraySpreadSlider");

    //stroke colour
    var stroke_redSlider = document.getElementById("stroke_redSlider");
    var stroke_greenSlider = document.getElementById("stroke_greenSlider");
    var stroke_blueSlider = document.getElementById("stroke_blueSlider");

    //SLIDER VALUES:
    //rgb
    var rValue = document.getElementById("rValue");
    var gValue = document.getElementById("gValue");
    var bValue = document.getElementById("bValue");

    //stroke
    var strokeValue = document.getElementById("strokeValue");

    //stamp
    var stampSizeSliderValue = document.getElementById("stampSizeSliderValue");

    //brush
    var brushSizeSliderValue = document.getElementById("brushSizeSliderValue");

    //spray
    var spraySizeSliderValue = document.getElementById("spraySizeSliderValue");
    var spraySpreadSliderValue = document.getElementById("spraySpreadSliderValue");

    //stroke colour
    var stroke_rValue = document.getElementById("stroke_rValue");
    var stroke_gValue = document.getElementById("stroke_gValue");
    var stroke_blue = document.getElementById("stroke_bValue");

    //SET SLIDER VALUES:
    //rgb values
    rValue.innerHTML = redSlider.value; // Display the default slider value
    gValue.innerHTML = greenSlider.value; // Display the default slider value
    bValue.innerHTML = blueSlider.value; // Display the default slider value

    //stroke value
    strokeValue.innerHTML = strokeSlider.value; // Display the default slider value

    //stamp value
    stampSizeSliderValue.innerHTML = stampSizeSlider.value; // Display the default slider value

    //brush value
    brushSizeSliderValue.innerHTML = brushSizeSlider.value; // Display the default slider value

    //spray values
    spraySizeSliderValue.innerHTML = spraySizeSlider.value; // Display the default slider value
    spraySpreadSliderValue.innerHTML = spraySpreadSlider.value; // Display the default slider value

    //stroke colour values
    stroke_rValue.innerHTML = stroke_redSlider.value; // Display the default slider value
    stroke_gValue.innerHTML = stroke_greenSlider.value; // Display the default slider value
    stroke_bValue.innerHTML = stroke_blueSlider.value; // Display the default slider value

    //UPDATE SLIDERS ON INPUT:
    //RGB SLIDER ---------------------------------------------------------
    // Update the current slider value (each time you drag the slider handle)
    redSlider.oninput = function () {

        if (typeof (rgb) == "string") {
            rgb = [0, 0, 0];
        }

        rValue.innerHTML = this.value;
        rgb[0] = parseInt(this.value);
    }

    // Update the current slider value (each time you drag the slider handle)
    greenSlider.oninput = function () {

        if (typeof (rgb) == "string") {
            rgb = [0, 0, 0];
        }

        gValue.innerHTML = this.value;
        rgb[1] = parseInt(this.value);
    }

    // Update the current slider value (each time you drag the slider handle)
    blueSlider.oninput = function () {

        if (typeof (rgb) == "string") {
            rgb = [0, 0, 0];
        }

        bValue.innerHTML = this.value;
        rgb[2] = parseInt(this.value);
    }

    //STROKE SLIDER ---------------------------------------------------------
    // Update the current slider value (each time you drag the slider handle)
    strokeSlider.oninput = function () {
        strokeValue.innerHTML = this.value;
        strokeWeight(this.value);
        strokeSize = this.value;
    }

    //STAMP SLIDER ---------------------------------------------------------
    // Update the current slider value (each time you drag the slider handle)
    stampSizeSlider.oninput = function () {
        stampSizeSliderValue.innerHTML = this.value;
        toolbox.selectedTool.stampSize = 30 * this.value;
    }
    // Update the current slider value (each time you drag the slider handle)
    brushSizeSlider.oninput = function () {
        brushSizeSliderValue.innerHTML = this.value;
        brushSize = this.value;
    }


    //SPRAY OPTIONS ---------------------------------------------------------
    // Update the current slider value (each time you drag the slider handle)
    spraySizeSlider.oninput = function () {
        spraySizeSliderValue.innerHTML = this.value;
        toolbox.selectedTool.size = this.value/10;
    }

    // Update the current slider value (each time you drag the slider handle)
    spraySpreadSlider.oninput = function () {
        spraySpreadSliderValue.innerHTML = this.value;
        toolbox.selectedTool.spread = parseInt(this.value);
    }

    //STROKE COLOUR ---------------------------------------------------------
    // Update the current slider value (each time you drag the slider handle)
    stroke_redSlider.oninput = function () {

        stroke_rValue.innerHTML = this.value;
        stroke_rgb[0] = parseInt(this.value);
    }

    // Update the current slider value (each time you drag the slider handle)
    stroke_greenSlider.oninput = function () {

        stroke_gValue.innerHTML = this.value;
        stroke_rgb[1] = parseInt(this.value);
    }

    // Update the current slider value (each time you drag the slider handle)
    stroke_blueSlider.oninput = function () {

        stroke_bValue.innerHTML = this.value;
        stroke_rgb[2] = parseInt(this.value);
    }

    // When stroke check-box is clicked on
    $("#strokeCheckbox").on("click", function () {
        // Get the checkbox
        var checkBox = document.getElementById("strokeCheckbox");
        // Get the output text
        var text = document.getElementById("text");

        // If checkbox is enabled, display options and preview border
        if (checkBox.checked == true) {
            document.getElementById("strokeOptions2").style.display = "block";
            document.getElementById("strokeColourSliders").style.display = "block";
        }
        else {
            document.getElementById("strokeOptions2").style.display = "none";
            document.getElementById("strokeColourSliders").style.display = "none";
        }
    });

    //Stamp click events
    $("#stamps").on("change", function () {
        var e = document.getElementById("stamps");
        var value = e.options[e.selectedIndex].value;

        //set image to selection option
        toolbox.selectedTool.img = toolbox.selectedTool.stamp[value];
    });
}
