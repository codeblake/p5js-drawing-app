//Line Tool
function LineTool() {
	this.icon = "media/icons/lineTool.png";  					// Icon path
	this.name = "lineTool";                     				// Tool name
	this.startingPos = {};

	// Draw Method
	this.draw = function() {
		if (mouseIsPressed) {                                   // On mouse press
			strokeWeight(brushSize);
			if (this.drawing) {                                 // When drawing enabled
				updatePixels();                                 // Update pixels
				this.width  += (mouseX - pmouseX);              // Update shape width from mouse input
				this.height += (mouseY - pmouseY);              // Update shape height from mouse input

				line(this.startingPos.x, this.startingPos.y, mouseX, mouseY);
			}
			else {
				this.startingPos.x = mouseX;                     // Set X starting position
				this.startingPos.y = mouseY;                     // Set Y starting position
				this.drawing       = true;                      // Enable drawing
			}
		}
		else {
			this.drawing = false;                               // Disable drawing
			this.width   = 20;                                  // Reset width
			this.height  = 20;                                  // Reset height
			loadPixels();                                       // Load the shape
		}
	};

	this.populateOptions = function() {
		strokeCap(SQUARE);										// Set stroke to hard edged

		//hide options
		document.getElementById("strokeOptions").style.display = "none";

		//show options
		document.getElementById("brushOptions").style.display = "block";
	};

	this.unselectTool = function() {
		//set stroke to round edged
		strokeCap(ROUND);

		//hide options
		document.getElementById("brushOptions").style.display = "none";

		//show options
		document.getElementById("strokeOptions").style.display = "block";

		loadPixels();
	};
}
