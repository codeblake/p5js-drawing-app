function BucketTool() {
    this.icon = 'media/icons/bucketTool.png';               // Icon path
    this.name = 'bucketTool';                               // Tool name

    // Draw Method
    this.draw = function () {
        if (mouseIsPressed) {                               // On mouse press
            let colour = get(mouseX, mouseY);               // Get target colour
            this.searchPixels(colour, mouseX, mouseY);      // Begin pixel search
        }
    };

    // Search Pixels for Target Colour
    this.searchPixels = function (targetColour, startingXpos, startingYpos) {
        let pixelColour = [];                               // Current pixel colour
        const pixelStack = [];                              // Pixel stack to recolour
        let x = startingXpos;                               // Staring X position
        let y = startingYpos;                               // Starting Y position

        rgb[3] = 255;                                       // Set new colour to full alpha (fix for alpha issue)
        targetColour[3]=255;                                // Set target colour to full alpha

        console.log(`Searching for ${targetColour}`);       // Output for debugging

        loadPixels();                                       // Load pixels

        // Search Top From Origin
        while (y >= 0) {

            // Search Left Side From Origin
            while (x >= 0) {
                const index = (x + y * canvas.width) * 4;   // Get pixel index for pixels array

                pixelColour = [                             // Set pixel colour
                    pixels[index + 0],                      // Red value
                    pixels[index + 1],                      // Green value
                    pixels[index + 2],                      // Blue value
                    pixels[index + 3]                       // Alpha value
                ];

                pixelColour[3]=255;                         // Change alpha value to fix alpha issue

                if (_.isEqual(pixelColour, targetColour)) { // Check if pixel colour matches target color
                    pixelStack.push([x, y]);                // Add to pixel stack
                    x--;                                    // Move left to next pixel
                }
                else {                                      // Stop searching left side of current row when not target colour
                    break;
                }
            }

            x = startingXpos;                               // Reset X position back to origin

            // Search Right Side From Origin
            while (x <= canvas.width) {
                const index = (x + y * canvas.width) * 4;   // Get pixel index for pixels array

                pixelColour = [                             // Set pixel colour
                    pixels[index + 0],                      // Red value
                    pixels[index + 1],                      // Green value
                    pixels[index + 2],                      // Blue value
                    pixels[index + 3]                       // Alpha value
                ];

                pixelColour[3]=255;                         // Change alpha value to fix alpha issue

                if (_.isEqual(pixelColour, targetColour)) { // Check if pixel colour matches target color
                    pixelStack.push([x, y]);                // Add to pixel stack
                    x++;                                    // Move right to next pixel
                }
                else {                                      // Stop searching left side of current row when not target colour
                    break;
                }
            }

            // Stop Searching If No Matching Colour
            if (x == startingXpos) {                        // If X position didn't change
                break;
            }
            else {
                x = startingXpos;                           // Reset X position
                y--;                                        // Change row
            }
        }

        x = startingXpos;                                   // Reset X position back to origin
        y = startingYpos;                                   // Reset Y position back to origin

        // Search Bottom From Origin
        while (y <= canvas.height) {

            // search Left Side From Origin
            while (x >= 0) {
                const index = (x + y * canvas.width) * 4;   // Get pixel index for pixel array

                pixelColour = [
                    pixels[index + 0],                      // Red value
                    pixels[index + 1],                      // Green value
                    pixels[index + 2],                      // Blue value
                    pixels[index + 3]                       // Alpha
                ];

                pixelColour[3]=255;                         // Change alpha value to fix alpha issue

                if (_.isEqual(pixelColour, targetColour)) { // Check if pixel colour matches target color
                    pixelStack.push([x, y]);                // Add to pixel stack
                    x--;                                    // Move left to next pixel
                }
                else {                                      // Stop searching left side of current row when not target colour
                    break;
                }
            }

            x = startingXpos;                               // Reset X position back to origin

            // Search Right Side From Origin
            while (x <= canvas.width) {
                const index = (x + y * canvas.width) * 4;   // Get pixel index for pixels array

                pixelColour = [                             // Set pixel colour
                    pixels[index + 0],                      // Red value
                    pixels[index + 1],                      // Green value
                    pixels[index + 2],                      // Blue value
                    pixels[index + 3]                       // Alpha value
                ];

                pixelColour[3]=255;                         // Change alpha value to fix alpha issue

                if (_.isEqual(pixelColour, targetColour)) { // Check if pixel colour matches target color
                    pixelStack.push([x, y]);                // Add to pixel stack
                    x++;                                    // Move right to next pixel
                }
                else {                                      // Stop searching left side of current row when not target colour
                    break;
                }
            }

            // Stop Searching If No Matching Colour
            if (x == startingXpos) {                        // If X position didn't change
                break;
            }
            else {
                x = startingXpos;                           // Reset X position
                y++;                                        // Change row
            }
        }

        this.replacePixelColour(pixelStack);                // Replace pixels in pixel stack
    };

    this.replacePixelColour = function (stack) {
        col = color(rgb);                                   // Set rgb as new colour

        for (let i = 0; i < stack.length; i++) {            // Iterate through pixel stack
            set(stack[i][0], stack[i][1], rgb);             // Change pixel's colour
        }
        updatePixels();                                     // Update pixels to draw
    };

    //When selecting tool
    this.populateOptions = function () {
        // Hide unneeded options
        document.getElementById("strokeOptions").style.display = "none";
    };

    //When deselecting tool
    this.unselectTool = function () {
        // Enable options on exit
        document.getElementById("strokeOptions").style.display = "block";
    };
}
