function EraserTool() {
    this.icon = "media/icons/eraserTool.png";                           // Icon path
    this.name = "eraserTool";                                           // Tool name

    this.draw = function () {
        if (mouseIsPressed) {                                           // If the mouse is pressed
            strokeWeight(brushSize);                                    // Set stroke size
            stroke(255);                                                // Eraser colour
            line(pmouseX, pmouseY, mouseX, mouseY);                     // Draw line
        }
    };

    //When selecting tool
    this.populateOptions = function () {
        //hide options
        document.getElementById("fillOptions").style.display = "none";
        document.getElementById("strokeOptions").style.display = "none";

        //show options
        document.getElementById("brushOptions").style.display = "block";
    };

    //When deselecting tool
    this.unselectTool = function () {
        //show other options
        document.getElementById("fillOptions").style.display = "block";
        document.getElementById("strokeOptions").style.display = "block";
    };
}
