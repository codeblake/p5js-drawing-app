//Triangle tool
	function TriangleTool() {
	this.icon    = "media/icons/triangleTool.png";				// Icon path
	this.name    = "triangleTool";								// Tool name
	this.size    = 20;											// Default size
	this.drawing = false;                                		//Set drawing bool to false
	this.startingPos = {                                        // X & Y object position
		x: 0,
		y: 0
	};

	// Draw Method
	this.draw = function() {
		if (mouseIsPressed) {                                   // On mouse press
			if (this.drawing) {                                 // When drawing enabled
				updatePixels();                                 // Update pixels
				this.size  += (mouseX - pmouseX);               // Update shape size from mouse input

				triangle(this.startingPos.x - this.size,		// Point 1 - X position
						 this.startingPos.y + this.size / 2,	// Point 1 - Y position

						 this.startingPos.x,					// Point 2 - X position
						 this.startingPos.y - this.size,		// Point 2 - Y position

						 this.startingPos.x + (this.size),		// Point 3 - X position
						 this.startingPos.y + this.size / 2);	// Point 3 - Y position

			}
			else {
				this.startingPos.x = mouseX                     // Set X starting position
				this.startingPos.y = mouseY                     // Set Y starting position
				this.drawing       = true;                      // Enable drawing
			}
		}
		else {
			this.drawing = false;                               // Disable drawing
			this.size   = 20;                                   // Reset size
			loadPixels();                                       // Load the shape
		}
	};

	//When selecting tool
	this.populateOptions = function() {
		//Hide stroke option if unchecked previously
		if (document.getElementById("strokeCheckbox").checked == false) {
			document.getElementById("strokeOptions2").style.display = "none";
		}
		//Hide brush options
		document.getElementById("brushOptions").style.display = "none";
	};

	//When deselecting tool
	this.unselectTool = function() {
		//Show stroke options
		document.getElementById("strokeOptions2").style.display = "block";
		loadPixels();
	};
};
