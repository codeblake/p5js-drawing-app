function PencilTool() {
    this.icon = "media/icons/pencilTool.png";                           // Tool icon
    this.name = "PencilTool";                                           // Tool name

    this.draw = function () {
        if (mouseIsPressed) {                                           // If mouse is pressed
            scribble.scribbleLine(pmouseX, pmouseY, mouseX, mouseY);    // draw line (scribble lib)
        }
    };

    this.populateOptions = function () {

        //Set pencil styling
        scribble.roughness = 2;
        scribble.maxOffset = 1;

        //Hide unneeded options
        document.getElementById("strokeOptions").style.display = "none";
        document.getElementById("sprayOptions").style.display = "none";
        document.getElementById("brushOptions").style.display = "none";
    };

    this.unselectTool = function () {
        strokeWeight(strokeSize);

        //Show other options
        document.getElementById("strokeOptions").style.display = "block";
        document.getElementById("fillOptions").style.display = "block";
    };
}
