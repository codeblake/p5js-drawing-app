// Ellipse tool
function EllipseTool() {
	this.icon        = 'media/icons/ellipseTool.png';       	// Icon path
	this.name        = 'ellipseTool';                           // Tool name
	this.width       = 20;                                      // Default width of shape
	this.height      = 20;                                      // Default height of shape
	this.drawing     = false;                                   // Drawing boolean
	this.startingPos = {                                        // X/Y object position
		x: 0,
		y: 0
	};

	// Draw Method
	this.draw = function() {
		if (mouseIsPressed) {                                   // On mouse press
			if (this.drawing) {                                 // When drawing enabled
				updatePixels();                                 // Update pixels
				this.width  += (mouseX - pmouseX);              // Update shape width from mouse input
				this.height += (mouseY - pmouseY);              // Update shape height from mouse input

				ellipse(this.startingPos.x, this.startingPos.y, // X/Y
					    this.width,                             // width
					    this.height)                            // height
			}
			else {
				this.startingPos.x = mouseX                     // Set X starting position
				this.startingPos.y = mouseY                     // Set Y starting position
				this.drawing       = true;                      // Enable drawing
			}
		}
		else {
			this.drawing = false;                               // Disable drawing
			this.width   = 20;                                  // Reset width
			this.height  = 20;                                  // Reset height
			loadPixels();                                       // Load the shape
		}
	};

    //When selecting tool
	this.populateOptions = function() {
        //Hide if stroke previously unchecked
		if (document.getElementById('strokeCheckbox').checked === false) {
			document.getElementById('strokeOptions2').style.display = 'none';
		}
		document.getElementById('brushOptions').style.display = 'none';
	};

    //When deselecting tool
	this.unselectTool = function() {
		//hide/show options
		document.getElementById('strokeOptions2').style.display = 'block';
		loadPixels();
	};
}
