//Ellipse tool
function StampTool() {
    this.icon = "media/icons/stampTool.png";    // Icon path
    this.name = "stampTool";                    // Tool name
    this.stamp = [];                            // Array of stamp images
    this.img;                                   // Stamp image
    this.stampSize = 30;                        // Stamp size

    //Load images into stamp array
    for (let i = 0; i < 20; i++) {
        this.stamp.push(loadImage(`../media/stamps/stamp${i}.png`));
    }

    //Set default stamp
    this.img = this.stamp[0];

    //Object draw method
    this.draw = function () {
        this.stampSize = this.stampSize;        // set current stamp size

        //Draw image at mouse location
        image(this.img,
              mouseX-this.stampSize/2,
              mouseY-this.stampSize/2,
              this.stampSize,
              this.stampSize);
    };

    //When selecting tool
    this.populateOptions = function () {
        //Hide unneeded options
        document.getElementById("fillOptions").style.display = "none";
        document.getElementById("strokeOptions").style.display = "none";

        //Show options
        document.getElementById("stampOptions").style.display = "block";

        //Set to appropriate stamp size
        document.getElementById("strokeSlider").value = 50;
        document.getElementById("strokeValue").innerHTML = 50;
        strokeSize = 50; //set stamp size
    };

    //When deselecting tool
    this.unselectTool = function () {
        //hide options
        document.getElementById("stampOptions").style.display = "none";

        //show other options
        document.getElementById("fillOptions").style.display = "block";
        document.getElementById("strokeOptions").style.display = "block";
        loadPixels();

    };
};
