//This creates a tree tool function and bring in an icon from the folder where it is kept
function TreeTool()
{
    this.icon ="media/icons/treeTool.png";
    this.name ="FractalTreeTool";

    // this creates an array
    var trees = [];
// These are the declared variables 'size' and 'angle'. These are here to give customization access by giving access to sliders. Sliders are a very useful tool from p5js.dom library.
    var angle = 0;
    var sizes = 0;


//draw the object, set the stroke , set root
this.draw = function() {


// this translate is responsible for  positioning the trees 'root' in the center of canvas.
    translate(mouseX, mouseY, height);
	stroke(0);



// draw the tree and sliders.
    for(var tree_element_iteration = 0; tree_element_iteration < trees.length; tree_element_iteration++)
    {

        trees[tree_element_iteration].somethingishappening;// draw root and indicate its length.
    }
	var somethingishappening = this.recursive_tree(160);
}


// This is a recursive function that is responsible for drawing all the brach levels. This function will call itself within itself.
// this code will draw a line and the if it meets the length rotate a certain amount and draw another line.
    this.recursive_tree = function(len)
    {
        line(0, 0, 0, -len);
        translate(0, -len);
// this function will call itself within itself. this is repeated until len meets the specified conditions.
        if (len > 4)
        {
//the push and pop makes sures that the branches come back before branching to the other side. Rather than building on top of where we left off.
//in other words draw a line save where the line ends draw it child branch. Then restore to where the last branch ended.
            push();
            rotate(0.6);
            this.recursive_tree(len * 0.64);
            pop();
            push();
            rotate(-0.6);
            this.recursive_tree(len * 0.64);
            pop();

 //these dots show every split that takes place within the tree.
            fill(255, 100, 10, 70);//give the dot a fill
            noStroke();//give no stroke look
            ellipse(0,0,7,7);//dots
        }
    }
}



