//Spray Can tool
function SprayCanTool(){
    this.icon = "media/icons/sprayTool.png";            // Icon path
    this.name = "sprayCanTool";                         // Tool name
    this.points = 13;                                   // Spray points
    this.spread = 10;                                   // Spray spread
    this.size = 1;                                      // Size of spray

    this.draw = function(){
        //if the mouse is pressed paint on the canvas
        //spread describes how far to spread the paint from the mouse pointer
        //points holds how many pixels of paint for each mouse press.

        if(mouseIsPressed){                             // If mouse is pressed
            strokeWeight(this.size);                    // Set stroke size
            stroke(rgb);                                // Set stroke colour

            for(var i = 0; i < this.points; i++){       // Iterate through spray points
                //Draw points to random x/y coordinate, range set by spray spread
                point(random(mouseX-this.spread, mouseX + this.spread),
                    random(mouseY-this.spread, mouseY+this.spread));
            }
        }
    };

    this.populateOptions = function () {
        //hide options
        document.getElementById("strokeOptions").style.display = "none";

        //show options
        document.getElementById("sprayOptions").style.display = "block";

        //set size to slider value
        this.size = document.getElementById("spraySizeSlider").value/10;
        this.spread = parseInt(document.getElementById("spraySpreadSlider").value);
    };

    this.unselectTool = function () {
        //hide options
        document.getElementById("sprayOptions").style.display = "none";

        //show options
        document.getElementById("strokeOptions").style.display = "block";
        loadPixels();
    };
};