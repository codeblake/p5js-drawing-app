function FreehandTool() {
    this.icon = "media/icons/freehandTool.png";                             // Icon Path
    this.name = "freehandTool";                                             // Tool name

    this.draw = function () {
        if (mouseIsPressed) {                                               // When the mouse is pressed
            strokeWeight(brushSize);                                        // Set stroke size
            line(pmouseX, pmouseY, mouseX, mouseY);                         // Draw line
        }
    };

    this.populateOptions = function () {
        //hide unneeded options
        document.getElementById("strokeOptions").style.display = "none";
        document.getElementById("sprayOptions").style.display = "none";

        //show options
        document.getElementById("brushOptions").style.display = "block";
    };

    this.unselectTool = function () {
        //hide unneeded options
        document.getElementById("brushOptions").style.display = "none";

        //show other options
        document.getElementById("strokeOptions").style.display = "block";
        document.getElementById("fillOptions").style.display = "block";
    };
}
