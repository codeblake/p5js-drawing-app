function StarSprayTool() {
    this.icon = "media/icons/starSprayTool.png";                  // Icon path
    this.name = "starSprayTool";                                  // Tool name
    this.sides = 5;                                               // Star size
    this.spread = 50;                                             // Spread of star spray
    this.size = 2;                                                // Size of stars

    // Star method
    this.star = function (x, y, size) {
        let angle = TWO_PI / this.sides;
        let halfAngle = angle / 2;

        beginShape();
        for (let a = 0; a < TWO_PI; a += angle) {
            let sx = x + cos(a) * size;
            let sy = y + sin(a) * size;
            vertex(sx, sy);
            sx = x + cos(a + halfAngle) * (size / 4);
            sy = y + sin(a + halfAngle) * (size / 4);
            vertex(sx, sy);
        }
        endShape(CLOSE);
    }

    // Draw method
    this.draw = function () {
        if (mouseIsPressed) {                                      // If mouse is pressed
            stroke(rgb);                                           // Set stroke colour

            //get random x/y coordinates, range set by spray spread
            let x = random(mouseX - this.spread, mouseX + this.spread);
            let y = random(mouseY - this.spread, mouseY + this.spread);

            //draw star
            this.star(x, y, this.size*2);
        }
    }

    this.populateOptions = function () {
        //hide options
        document.getElementById("strokeOptions").style.display = "none";

        //show options
        document.getElementById("sprayOptions").style.display = "block";

        //set size to slider value
        this.size = document.getElementById("spraySizeSlider").value/10;
        this.spread = parseInt(document.getElementById("spraySpreadSlider").value);
    };

    this.unselectTool = function () {
        //hide options
        document.getElementById("sprayOptions").style.display = "none";
        loadPixels();
    };
};
