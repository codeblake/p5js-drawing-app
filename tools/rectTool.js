//Rectangle tool
function RectTool(){
    this.icon = "media/icons/rectTool.png";	// Icon path
    this.name = "rectTool";					// Tool name

	var startMouseX = -1;                	// Set mouse x starting position
	var startMouseY = -1;                	// Set mouse y starting position
	var drawing = false;                 	// Set drawing bool to false

	this.draw = function(){              	// Object draw method

		if(mouseIsPressed){              	// When mouse pressed...
			if(startMouseX == -1){       	// and in starting x position...
				startMouseX = mouseX;    	// update starting mouse x position
				startMouseY = mouseY;    	// update starting mouse y position
				drawing = true;          	// enable drawing boolean
				loadPixels();            	// load pixel data for drawing
			}

			else{
				updatePixels();          	// update display with pixel data
				rect(startMouseX, startMouseY,
                     mouseX-startMouseX,
                     mouseY-startMouseY);	// draw a rect from starting mouse x/y pos to mouse/xy
			}
		}

		else if(drawing){                	// when drawing is true...
			drawing = false;             	// set drawing to false
			startMouseX = -1;            	// set starting mouse x position
			startMouseY = -1;            	// set starting mouse y position
		}
	};

    this.populateOptions = function () {
        //Hide if stroke previously unchecked
        if(document.getElementById("strokeCheckbox").checked == false){
           document.getElementById("strokeOptions2").style.display = "none";
        }
        document.getElementById("brushOptions").style.display = "none";
    };

    this.unselectTool = function () {
        //show options
        document.getElementById("strokeOptions2").style.display = "block";
        loadPixels();
    };
};