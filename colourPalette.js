var fillColour = 0;

//Displays and handles the colour palette.
function ColourPalette() {
	//a list of web colour strings
	this.colours = ["black", "white", "silver", "gray", "red", "maroon", "purple", "orange", "pink", "fuchsia", "lime", "green", "olive", "yellow", "navy", "blue", "teal", "aqua", "salmon", "brown", "tan"];

	//set starting colour be black
	this.selectedColour = "black";

	//load in the colours
	this.loadColours = function() {
		//set the fill and stroke properties to be black at the start of the programme
		//running
		fill(this.colours[0]);
		stroke(this.colours[0]);

		//for each colour create a new div in the html for the colourSwatches
		for (var i = 0; i < this.colours.length; i++) {
			var colourID = this.colours[i] + "Swatch";
			var colourHTML = "<div class='colourSwatches' id='" + colourID + "'></div>";
			//using JQuery add the swatch to the palette and set its background colour
			//to be the colour value.
			$(".colourPalette").append(colourHTML);
			$("#" + colourID).css("background-color", this.colours[i]);
		}

		$(".colourSwatches").first().css("border", "2px solid blue");
	};

	//call the loadColours function now it is declared
	this.loadColours();

	//handle clicks on the colours.
	$(".colourPalette").on("click", ".colourSwatches", function() {
		//get the colour string back from the id
		var id = $(this).attr("id");
		var c = split(id, "Swatch")[0];

		//set the selected colour and the fill and stroke
		this.selectedColour = c;
		fillColour = c;
		rgb = fillColour;
		fill(c);
		stroke(c);

		//remove the boarder from the last selected swatch (check them all and remove it)
		$(".colourSwatches").css("border", "0");

		//add a border to the clicked swatch div.
		$(this).css("border", "2px solid blue");


		//set sliders to swatch values
		switch (c) {
			case "black":
				rValue.innerHTML = 0;
				gValue.innerHTML = 0;
				bValue.innerHTML = 0;

				redSlider.value = 0;
				greenSlider.value = 0;
				blueSlider.value = 0;

				rgb = [0, 0, 0];
				break;

			case "white":
				rValue.innerHTML = 255;
				gValue.innerHTML = 255;
				bValue.innerHTML = 255;

				redSlider.value = 255;
				greenSlider.value = 255;
				blueSlider.value = 255;

				rgb = [255, 255, 255];
				break;

			case "gray":
				rValue.innerHTML = 128;
				gValue.innerHTML = 128;
				bValue.innerHTML = 128;

				redSlider.value = 128;
				greenSlider.value = 128;
				blueSlider.value = 128;

				rgb = [128, 128, 128];
				break;

			case "silver":
				rValue.innerHTML = 192;
				gValue.innerHTML = 192;
				bValue.innerHTML = 192;

				redSlider.value = 192;
				greenSlider.value = 192;
				blueSlider.value = 192;

				rgb = [192, 192, 192];
				break;

			case "maroon":
				rValue.innerHTML = 128;
				gValue.innerHTML = 0;
				bValue.innerHTML = 0;

				redSlider.value = 128;
				greenSlider.value = 0;
				blueSlider.value = 0;

				rgb = [128, 0, 0];
				break;

			case "red":
				rValue.innerHTML = 255;
				gValue.innerHTML = 0;
				bValue.innerHTML = 0;

				redSlider.value = 255;
				greenSlider.value = 0;
				blueSlider.value = 0;

				rgb = [255, 0, 0];
				break;

			case "purple":
				rValue.innerHTML = 128;
				gValue.innerHTML = 0;
				bValue.innerHTML = 128;

				redSlider.value = 128;
				greenSlider.value = 0;
				blueSlider.value = 128;

				rgb = [128, 0, 128];
				break;

			case "orange":
				rValue.innerHTML = 255;
				gValue.innerHTML = 165;
				bValue.innerHTML = 0;

				redSlider.value = 255;
				greenSlider.value = 165;
				blueSlider.value = 0;

				rgb = [255, 165, 0];
				break;

			case "pink":
				rValue.innerHTML = 255;
				gValue.innerHTML = 192;
				bValue.innerHTML = 203;

				redSlider.value = 255;
				greenSlider.value = 192;
				blueSlider.value = 203;

				rgb = [255, 192, 203]
				break;

			case "fuchsia":
				rValue.innerHTML = 255;
				gValue.innerHTML = 0;
				bValue.innerHTML = 255;

				redSlider.value = 255;
				greenSlider.value = 0;
				blueSlider.value = 255;

				rgb = [255, 0, 255]
				break;

			case "green":
				rValue.innerHTML = 0;
				gValue.innerHTML = 128;
				bValue.innerHTML = 0;

				redSlider.value = 0;
				greenSlider.value = 128;
				blueSlider.value = 0;

				rgb = [0, 128, 0];
				break;

			case "lime":
				rValue.innerHTML = 0;
				gValue.innerHTML = 255;
				bValue.innerHTML = 0;

				redSlider.value = 0;
				greenSlider.value = 255;
				blueSlider.value = 0;

				rgb = [0, 255, 0];
				break;

			case "olive":
				rValue.innerHTML = 128;
				gValue.innerHTML = 128;
				bValue.innerHTML = 0;

				redSlider.value = 128;
				greenSlider.value = 128;
				blueSlider.value = 0;

				rgb = [128, 128, 0];
				break;

			case "yellow":
				rValue.innerHTML = 255;
				gValue.innerHTML = 255;
				bValue.innerHTML = 0;

				redSlider.value = 255;
				greenSlider.value = 255;
				blueSlider.value = 0;

				rgb = [255, 255, 0];
				break;

			case "navy":
				rValue.innerHTML = 0;
				gValue.innerHTML = 0;
				bValue.innerHTML = 128;

				redSlider.value = 0;
				greenSlider.value = 0;
				blueSlider.value = 128;

				rgb = [0, 0, 128];
				break;

			case "blue":
				rValue.innerHTML = 0;
				gValue.innerHTML = 0;
				bValue.innerHTML = 255;

				redSlider.value = 0;
				greenSlider.value = 0;
				blueSlider.value = 255;

				rgb = [0, 0, 255];
				break;

			case "teal":
				rValue.innerHTML = 0;
				gValue.innerHTML = 128;
				bValue.innerHTML = 128;

				redSlider.value = 0;
				greenSlider.value = 128;
				blueSlider.value = 128;

				rgb = [0, 128, 128];
				break;

			case "aqua":
				rValue.innerHTML = 0;
				gValue.innerHTML = 255;
				bValue.innerHTML = 255;

				redSlider.value = 0;
				greenSlider.value = 255;
				blueSlider.value = 255;

				rgb = [0, 255, 255];
				break;

			case "salmon":
				rValue.innerHTML = 250;
				gValue.innerHTML = 128;
				bValue.innerHTML = 114;

				redSlider.value = 250;
				greenSlider.value = 128;
				blueSlider.value = 114;

				rgb = [150, 128, 114];
				break;

			case "brown":
				rValue.innerHTML = 165;
				gValue.innerHTML = 42;
				bValue.innerHTML = 42;

				redSlider.value = 165;
				greenSlider.value = 42;
				blueSlider.value = 42;

				rgb = [165, 42, 42];
				break;

			case "tan":
				rValue.innerHTML = 210;
				gValue.innerHTML = 180;
				bValue.innerHTML = 140;

				redSlider.value = 210;
				greenSlider.value = 180;
				blueSlider.value = 140;

				rgb = [210, 180, 140];
				break;
		}
	});
}
