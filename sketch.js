// global variables that will store the toolbox colour palette
// and the helper functions
let toolbox  = null;
let colourP  = null;
let helpers  = null;
let scribble = null;
// globals
let brushSize = null;
let test      = "test"

// UI checking
function notOnUI() {
    return (mouseX > 0 && mouseX < innerWidth - 200 && mouseY > 0 && mouseY < innerHeight - 100);
}

function setup() {
    // create a canvas to fill the content div from index.html
    canvasContainer = $('#content');
    let c = createCanvas(canvasContainer.innerWidth(), canvasContainer.innerHeight());

    c.parent('content');    //setting content

    // create helper functions and the colour palette
    helpers  = new HelperFunctions();
    colourP  = new ColourPalette();
    scribble = new Scribble();         // adding scribble for pencil tool

    // create a toolbox for storing the tools
    toolbox = new Toolbox();

    // add the tools to the toolbox.
    toolbox.addTool(new PencilTool());
    toolbox.addTool(new FreehandTool());
    toolbox.addTool(new SprayCanTool());
    toolbox.addTool(new StarSprayTool());
    toolbox.addTool(new LineTool());
    toolbox.addTool(new RectTool());
    toolbox.addTool(new EllipseTool());
    toolbox.addTool(new TriangleTool());
    toolbox.addTool(new StampTool());
    toolbox.addTool(new MirrorDrawTool());
    toolbox.addTool(new BucketTool());
    toolbox.addTool(new EraserTool());
    toolbox.addTool(new TreeTool());

    pixelDensity(1);    //set pixel density

    background(255);    //background colour
}

function draw() {
    // call the draw function from the selected tool.
    // hasOwnProperty is a javascript function that tests
    // if an object contains a particular method or property
    // if there isn't a draw method the app will alert the user
    if (toolbox.selectedTool.hasOwnProperty('draw')) {
        // To fix color switching from eraser to pencil
        if (typeof (rgb) === 'string') {
            stroke(fillColour);
        }
        else {
            // update stroke weight
            strokeWeight(strokeSize);

            // update fill colour
            fill(rgb);

            // When stroke enabled: set stroke colour to stroke RGB
            if (document.getElementById('strokeCheckbox').checked == true) {
                // update stroke colour
                stroke(stroke_rgb);

                // update preview stroke colour
                colourPreview.style.borderColor = `rgb(${  stroke_rgb[0]  },${  stroke_rgb[1]  },${  stroke_rgb[2]  })`;
                colourPreview.style.borderWidth = `${strokeSize/3}px`;
            }
            // If stroke disabled...
            else {
                // set stroke colour to fill colour
                stroke(rgb);

                // set preview box border to fill colour
                colourPreview.style.borderColor = `rgb(${  rgb[0]  },${  rgb[1]  },${  rgb[2]  })`;
            }

            // update preview fill colour
            colourPreview.style.backgroundColor = `rgb(${  rgb[0]  },${  rgb[1]  },${  rgb[2]  })`;
        }

        // When Spray tools
        if (toolbox.selectedTool.name == 'starSprayTool' || toolbox.selectedTool.name == 'sprayCanTool') {
            strokeWeight(1);
        }
        // When other tools
        else if (toolbox.selectedTool.name == 'freehandTool' || toolbox.selectedTool.name == 'LineTool') {
            strokeWeight(brushSize);
            stroke(rgb);
        }

        if (toolbox.selectedTool.name != 'stampTool' && toolbox.selectedTool.name != 'bucketTool') {
            // start drawing when off UI
            if (notOnUI()) {
                toolbox.selectedTool.draw();
            }
            // to fix mirror option update
            else if (toolbox.selectedTool.name == 'mirrorDraw') {
                toolbox.selectedTool.draw();
            }
        }
    }
    else {
        alert("it doesn't look like your tool has a draw method!");
    }
}

//When mouse is pressed
function mousePressed() {
    //If not on UI, use tools
    if (notOnUI()) {
        if (toolbox.selectedTool.name == 'stampTool' || toolbox.selectedTool.name == 'bucketTool') {
            toolbox.selectedTool.draw();
        }
    }
}

//When mouse is dragged
function mouseDragged() {
    //When triangle tool and mouse dragged, update triangle size
    if (toolbox.selectedTool.name == 'triangleTool') {
        if (mouseY > pmouseY) {
            toolbox.selectedTool.size += 5;
        }
        else if (toolbox.selectedTool.size > 5) {
            toolbox.selectedTool.size -= 5;
        }
    }
}
